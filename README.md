Bitbucket Repo Sync Application
===============================

Bitbucket Repo Sync is a small [Symfony Console](http://symfony.com/doc/current/components/console.html) application to keep all your bitbucket git repositories in synced. It utilizes [Bitbucket API](https://api.bitbucket.org/).

Installation and Usage
----------------------

Basically, you give your Bitbucket credentials and it clones all your git repositories.

    git clone git@bitbucket.org:eabay/bitbucket-repo-sync.git
    cd bitbucket-repo-sync
    // see http://getcomposer.org/download/ to install composer
    php composer.phar install
    php console.php bitbucket:sync username password

You can choose the path of repositories:

    php console.php bitbucket:sync username password -d /some/where

To get help about the command:

    php console.php help bitbucket:sync

**Notice that repositories are cloned over HTTPS schema including your username and password. I mean repositories will have origins with URIs like _https://username:password@bitbucket.org/…_. If this is a security problem for you, don't use the application!**

Please use [Bitbucket](https://bitbucket.org/eabay/bitbucket-repo-sync/issues) to file issues.

<?php

namespace Bitbucket\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('bitbucket:sync')
            ->setDescription('Sync bitbucket repos of user using API')
            ->addArgument('username', InputArgument::REQUIRED, 'Bitbucket username')
            ->addArgument('password', InputArgument::REQUIRED, 'Bitbucket password')
            ->addOption('timeout', 't', InputOption::VALUE_OPTIONAL, 'Connection timeout', 10)
            ->addOption('repo-dir', 'd', InputOption::VALUE_OPTIONAL, 'Directory where repositories cloned', getcwd().'/repos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write('<comment>Querying API...</comment>');

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://api.bitbucket.org/1.0/user/repositories/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_USERPWD => sprintf('%s:%s', $input->getArgument('username'), $input->getArgument('password')),
            CURLOPT_TIMEOUT => $input->getOption('timeout'),
        ));

        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);

        $output->write(array('<info>done</info>', "\n"));

        if ($response['http_code'] == 200) {
            $repos = json_decode($content);
            $repoDir = $input->getOption('repo-dir');

            $output->writeln(array(
                sprintf('<question>%d</question> repositories found.', count($repos)),
                sprintf('Directory for repositories: <comment>%s</comment>', $repoDir),
            ));

            if (!is_dir($repoDir) && !mkdir($repoDir, 0700, true)) {
                throw new \RuntimeException(sprintf('Cannot create path %s', $repoDir));
            }

            foreach ($repos as $repo) {
                $gitUri = sprintf('https://%s:%s@bitbucket.org/%s/%s.git', $input->getArgument('username'), $input->getArgument('password'), $repo->owner, $repo->slug);
                $repoPath = $repoDir.'/'.$repo->slug;

                $output->writeln(array(
                str_repeat('-', 100),
                    sprintf("Git URI\t: <info>%s</info>", $gitUri),
                    sprintf("Path\t: <info>%s</info>", $repoPath),
                ));

                $command = sprintf('git clone %s %s', $gitUri, $repoPath);

                if (is_dir($repoPath.'/.git')) {
                    $command = sprintf('cd %s && git reset --hard HEAD && git pull', $repoPath);
                }

                $output->writeln(sprintf('<comment>Running command</comment> <question>%s</question>', $command));

                $output->writeln(exec($command));
            }
        } else {
            $output->writeln(array(
                '<error>Bad HTTP response</error>',
                sprintf('<info>%s</info>', print_r($response, true)),
            ));
        }
    }
}

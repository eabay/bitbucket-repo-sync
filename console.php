<?php

require_once __DIR__ . '/vendor/autoload.php';

$console = new \Symfony\Component\Console\Application('Bitbucket Repo Sync Application', '1.0');
$console->add(new \Bitbucket\Command\SyncCommand());
$console->run();
